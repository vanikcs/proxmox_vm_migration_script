# PVE VM migration util
## Quick start guide

**Developed on Python version 3.10**

### Preparing host
1. Edit `/etc/hosts` file, add records for your vms
2. Copy ssh-id to target host
3. Fill `hosts_state.json`
### Preparing python env
1. Install python and pip
    ```
    apt install -y python3
    apt install -y pip
    ```
2. Create virtual environment
    ```
    mkdir venv
    python3 -m venv venv
    source venv/bin/activate
    ```
3. Install requirements
    ```
    pip install -r requirements.txt
    ```
    or without venv
    ```
    apt install -y python3-paramiko
    apt install -y python3-scp
    apt install -y python3-jsonschema
    ```
1. Start using tool
    ```
    python3 migrate.py
    ```
