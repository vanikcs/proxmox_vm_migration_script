import os
import json
import sys
from pathlib import Path
import re
import subprocess
import logging
import jsonschema
from paramiko import SSHClient, AutoAddPolicy
from scp import SCPClient


class SSHConnection:
    def __init__(self, hostname):
        self._client_ssh = SSHClient()
        self.__key_filepath = str(Path.home() / ".ssh" / "id_rsa")
        self._client_ssh.set_missing_host_key_policy(AutoAddPolicy())
        self._client_ssh.connect(
            hostname, username="root", key_filename=self.__key_filepath
        )
        self._client_sftp = self._client_ssh.open_sftp()

    def _progress4(self, filename: bytes, size: int, sent: int, peername: tuple):
        sys.stdout.write(
            "(%s:%s) %s's progress: %.2f%% \r"
            % (peername[0], peername[1], filename.decode(), float(sent) / float(size) * 100)
        )

    def __del__(self):
        if self._client_sftp:
            self._client_sftp.close()
        if self._client_ssh:
            self._client_ssh.close()

    def _check_backup_remote(self, backup_name: str):
        try:
            self._client_sftp.stat(f"/var/lib/vz/dump/{backup_name}")
            logging.warning(f"Backup {backup_name} exist on remote host!")
            return True
        except FileNotFoundError:
            return False

    def migrate_backup(self, backup_name_list: list):
        for backup_name in backup_name_list:
            is_backup_exist = self._check_backup_remote(backup_name)
            if is_backup_exist is False:
                logging.warning(f"Backup {backup_name} not found on remote host!")
                with SCPClient(
                    self._client_ssh.get_transport(), progress4=self._progress4
                ) as scp:
                    scp.put(f"/var/lib/vz/dump/{backup_name}", "/var/lib/vz/dump/")
                print(f"Backup {backup_name} copied!")

    def create_vm(self, backup_name_list: str):
        for backup_name in backup_name_list:
            match = re.search(r"\d+", backup_name)
            vm_id = match.group()
            _, _, stderr = self._client_ssh.exec_command(
                f"qmrestore /var/lib/vz/dump/{backup_name} {vm_id}"
            )
            output_err = stderr.read()
            if output_err:
                logging.error(f"{output_err.decode()}")
            else:
                print(f"Template vm {vm_id} created!")


def validate_state(file_path: str):
    with open(file_path) as schema_file:
        schema = json.load(schema_file)
    jsonschema.validate(instance=file_path, schema=schema)
    print(f"File {file_path} validate successfully!")


def get_hosts_state(file_path: str) -> dict:
    try:
        with open(file_path, "r") as file:
            data = json.load(file)
            print(f"Data got successfully from {file_path}!")
            return data
    except Exception as error:
        logging.error(f"Reading file error: {error}!")
        raise error


def check_backup(vm_id: int, pattern: str) -> str:
    for file_name in os.listdir("/var/lib/vz/dump/"):
        result = re.search(pattern, file_name)
        if result:
            logging.warning(f"Backup for {vm_id} exist!")
            return result.group()
    else:
        print(f"Creating backup for {vm_id}!")


def create_backups(hosts_state: dict) -> dict:
    avaliable_backups = []
    avaliable_state = {}
    for pve_name, vm_ids in hosts_state.items():
        for vm_id in vm_ids:
            pattern = (
                f"^vzdump-qemu-{str(vm_id)}"
                + r"-[0-9]{4}_[0-9]{2}_[0-9]{2}-[0-9]{2}_[0-9]{2}_[0-9]{2}.vma.[a-z]+$"
            )
            backup_name = check_backup(vm_id, pattern)

            if not backup_name:
                backup_command = f"vzdump --mode stop --compress zstd {vm_id}"
                subprocess.run(backup_command, shell=True)
                backup_name = check_backup(vm_id, pattern)
            avaliable_backups.append(backup_name)
            avaliable_state.update({pve_name: avaliable_backups})
    return avaliable_state


def ssh_tasks(available_state: dict):
    for pve_name, vm_list in available_state.items():
        ssh = SSHConnection(pve_name)
        ssh.migrate_backup(vm_list)
        ssh.create_vm(vm_list)


def main():
    logging.basicConfig(level=logging.WARNING)
    file_path = "./hosts_state.json"
    validate_state(file_path)
    required_state = get_hosts_state(file_path)
    avaliable_state = create_backups(required_state)
    ssh_tasks(avaliable_state)


if __name__ == "__main__":
    main()
